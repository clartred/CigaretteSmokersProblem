import java.util.Random;

/**
 * Agent is a class which provides two ingredients for smoking men.
 * @author  Nemanja Rajkovic
 * @date 13.2.2018.
 */
public class Agent implements Runnable {

    private Random random;

    public Agent() {
        random = new Random();
    }

    /**
     * This method will give 2 ingredients to the rest of
     * the smoking men. It will function as a Semaphore (Java custom made Semaphore)
     * and will tell one of the three threads to continue with their work by setting
     * setSemaphore(true).
     */
    public void giveIngredients() {

        int randomNumber = random.nextInt(3) + 1;

        if (randomNumber == 1) {
            Matches.setMatches(true);
            Tobacco.setTobacco(true);
            System.out.println("Agent gave Matches and Tobacco\n");
            Paper.setSemaphore(true);
        } else if (randomNumber == 2) {
            Matches.setMatches(true);
            Paper.setPaper(true);
            System.out.println("Agent gave Matches and Paper\n");
            Tobacco.setSemaphore(true);
        } else {
            Paper.setPaper(true);
            Tobacco.setTobacco(true);
            System.out.println("Agent gave Paper and Tobacco\n");
            Matches.setSemaphore(true);
        }
    }

    @Override
    public void run() {
        while (true) {
            if (!Matches.isMatches() && !Tobacco.isTobacco() && !Paper.isPaper()) {
                try {
                    //This .sleep() is here because otherwise, people are smoking way too fast!
                    //console will write too fast for user to read properly.
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                giveIngredients();
            }
        }
    }
}
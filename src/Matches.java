/**
 * This class represents Matches-Man in Cigarette Smokers Pattern.
 * Note that all  getters and setters in this class are synchronized
 * because they are being called from various threads.
 *
 * @author Nemanja Rajkovic
 * @date 13.2.2018.
 */
public class Matches implements Runnable {

    private static boolean matches;
    private static boolean semaphore;

    /**
     * This method will "consume" ingredients and disable semaphore so it wont be called
     * again without Agent notifying it.
     */
    public void smoke() {

        System.out.println("Man with Matches just took all ingredients and now will smoke a cigar! ");
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setMatches(false);
        Paper.setPaper(false);
        Tobacco.setTobacco(false);
        setSemaphore(false);
        System.out.println("Man with Matches finished smoking.\n");
    }


    @Override
    public void run() {
        while (true) {
            if (isSemaphore()) {
                smoke();
            }
        }
    }

    public synchronized static void setMatches(boolean matches) {
        Matches.matches = matches;
    }

    public synchronized static void setSemaphore(boolean semaphore) {
        Matches.semaphore = semaphore;
    }

    public synchronized static boolean isMatches() {
        return matches;
    }

    public synchronized static boolean isSemaphore() {
        return semaphore;
    }
}
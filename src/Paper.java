/**
 * This class represents Paper-Man in Cigarette Smokers Pattern.
 * Note that all  getters and setters in this class are synchronized
 * because they are being called from various threads.
 *
 * @author Nemanja Rajkovic
 * @date 13.2.2018.
 */
public class Paper implements Runnable {

    private static boolean paper;
    private static boolean semaphore;

    /**
     * This method will "consume" ingredients and disable semaphore so it wont be called
     * again without Agent notifying it.
     */
    public void smoke() {
        System.out.println("Man with Paper just took all ingredients and now will smoke a cigar! ");
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setPaper(false);
        Tobacco.setTobacco(false);
        Matches.setMatches(false);
        setSemaphore(false);
        System.out.println("Man with Paper finished smoking.\n");
    }


    @Override
    public void run() {
        while (true) {
            if (isSemaphore()) {
                smoke();
            }
        }
    }

    public synchronized static void setSemaphore(boolean semaphore) {
        Paper.semaphore = semaphore;
    }

    public synchronized static void setPaper(boolean paper) {
        Paper.paper = paper;
    }

    public synchronized static boolean isPaper() {
        return paper;
    }

    public synchronized static boolean isSemaphore() {
        return semaphore;
    }
}
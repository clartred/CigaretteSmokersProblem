/**
 * This class represents Tobacco-Man in Cigarette Smokers Pattern.
 * Note that all  getters and setters in this class are synchronized
 * because they are being called from various threads.
 *
 * @author Nemanja Rajkovic
 * @date 13.2.2018.
 */
public class Tobacco implements Runnable {

    private static boolean tobacco;
    private static boolean semaphore;

    /**
     * This method will "consume" ingredients and disable semaphore so it wont be called
     * again without Agent notifying it.
     */
    public void smoke() {
        System.out.println("Man with Tobacco just took all ingredients and now will smoke a cigar! ");
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setTobacco(false);
        Paper.setPaper(false);
        Matches.setMatches(false);
        setSemaphore(false);
        System.out.println("Man with Tobacco finished smoking.\n");
    }

    @Override
    public void run() {
        while (true) {
            if (isSemaphore()) {
                smoke();
            }
        }
    }

    public synchronized static void setTobacco(boolean tobacco) {
        Tobacco.tobacco = tobacco;
    }

    public synchronized static void setSemaphore(boolean semaphore) {
        Tobacco.semaphore = semaphore;
    }

    public synchronized static boolean isTobacco() {
        return tobacco;
    }

    public synchronized static boolean isSemaphore() {
        return semaphore;
    }
}
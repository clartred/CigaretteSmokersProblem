public class Main {

    public static void main(String args[]) {

        Agent agent = new Agent();
        Matches matches = new Matches();
        Tobacco tobacco = new Tobacco();
        Paper paper = new Paper();

        Thread agentThread = new Thread(agent);
        Thread matchesThread = new Thread(matches);
        Thread tobaccoThread = new Thread(tobacco);
        Thread paperThread = new Thread(paper);

        agentThread.start();
        matchesThread.start();
        tobaccoThread.start();
        paperThread.start();
    }
}